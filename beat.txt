in_thread do
  sleep 2
  2.times do
    sample :drum_heavy_kick, amp: 0.5
    sleep 0.5
    sample :drum_heavy_kick, amp: 0.5
    sleep 0.5
    sample :drum_snare_hard, amp: 0.2
    sleep 0.5
    sample :drum_heavy_kick, amp: 0.5
    sleep 1
    sample :drum_heavy_kick, amp: 0.5
    sleep 0.5
    sample :drum_snare_hard, amp: 0.2
    sleep 1
  end
  sleep 4
  4.times do
    sample :drum_heavy_kick, amp: 0.5
    sleep 0.5
    sample :drum_heavy_kick, amp: 0.5
    sleep 0.5
    sample :drum_snare_hard, amp: 0.2
    sleep 0.5
    sample :drum_heavy_kick, amp: 0.5
    sleep 1
    sample :drum_heavy_kick, amp: 0.5
    sleep 0.5
    sample :drum_snare_hard, amp: 0.2
    sleep 1
  end
end

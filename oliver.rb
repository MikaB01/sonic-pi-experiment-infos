in_thread do
  use_synth :dark_ambience
  5.times do
    6.times do
      play :C, amp:1, release: 1.9
      sleep 1
    end
  end
end
in_thread do
  a = 1.3
  f = 0.9
  use_synth :piano
  5.times do
    2.times do
      play [:C2, :E2, :G2], amp: a, release: 1.9
      sleep 1
      a = a * 0.98
      play [:C2, :E2, :G2], amp: a, release: 1.9
      sleep 1
      a = a * 0.98
      play [:G1, :C2, :E2], amp: a, release: 1.9
      sleep 1
      a *= f
    end
  end
end
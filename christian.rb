tick          = 1.0
half          = 0.5*tick
quart         = 0.25*tick
eigth         = 0.125*tick
length        = 8*tick
define :bass_guitar do
  use_synth :piano
  play :c
  sleep half
  play :d
  sleep quart
  play :e, release: 0.4
  sleep quart
  play :f, release: 0.4
  sleep quart
  play :g, release: 0.4
  sleep quart
  play :g, release: 0.8
  sleep tick
end
define :permanent_drumset do
  freak_out_mode = [true, false, false].sample
  length.to_i.times.each_with_index do |_, i|
    sample :drum_bass_soft
    sample :drum_cymbal_closed
    sleep half
  end
end
in_thread(name: :frame) do
  cue :frame
  12.times do
    permanent_drumset
  end
end
in_thread do
  12.times do
    bass_guitar
  end
end
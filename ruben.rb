with_fx :reverb, room: 1 do
  with_fx :echo do
    with_synth :hollow do
      live_loop :spooky do
        play rrand(50, 70), cutoff: rrand(70, 100), amp: rrand(1, 2.5)
        sleep rrand(0.75, 1.5)
      end
    end
  end
end
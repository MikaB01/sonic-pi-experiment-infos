in_thread do
  sleep 2
  2.times do
    sample :drum_heavy_kick, amp: 0.5
    sleep 0.5
    sample :drum_heavy_kick, amp: 0.5
    sleep 0.5
    sample :drum_snare_hard, amp: 0.2
    sleep 0.5
    sample :drum_heavy_kick, amp: 0.5
    sleep 1
    sample :drum_heavy_kick, amp: 0.5
    sleep 0.5
    sample :drum_snare_hard, amp: 0.2
    sleep 1
  end
  sleep 4
  4.times do
    sample :drum_heavy_kick, amp: 0.5
    sleep 0.5
    sample :drum_heavy_kick, amp: 0.5
    sleep 0.5
    sample :drum_snare_hard, amp: 0.2
    sleep 0.5
    sample :drum_heavy_kick, amp: 0.5
    sleep 1
    sample :drum_heavy_kick, amp: 0.5
    sleep 0.5
    sample :drum_snare_hard, amp: 0.2
    sleep 1
  end
end

#Oliver
in_thread do
  use_synth :dark_ambience
  5.times do
    6.times do
      play :C, amp:1, release: 1.9
      sleep 1
    end
  end
end
in_thread do
  a = 1.3
  f = 0.9
  use_synth :piano
  5.times do
    2.times do
      play [:C2, :E2, :G2], amp: a, release: 1.9
      sleep 1
      a = a * 0.98
      play [:C2, :E2, :G2], amp: a, release: 1.9
      sleep 1
      a = a * 0.98
      play [:G1, :C2, :E2], amp: a, release: 1.9
      sleep 1
      a *= f
    end
  end
end

#Ruben
in_thread do
  with_fx :reverb, room: 1 do
    with_fx :echo do
      with_synth :hollow do
        live_loop :spooky do
          play rrand(50, 70), cutoff: rrand(70, 100), amp: rrand(1, 2.5)
          sleep rrand(0.75, 1.5)
        end
      end
    end
  end
end

#Christian
in_thread do
  tick          = 1.0
  half          = 0.5*tick
  quart         = 0.25*tick
  eigth         = 0.125*tick
  length        = 8*tick
  define :bass_guitar do
    use_synth :piano
    play :c, amp: 0.3
    sleep half
    play :d, amp: 0.3
    sleep quart
    play :e, release: 0.4, amp: 0.3
    sleep quart
    play :f, release: 0.4, amp: 0.3
    sleep quart
    play :g, release: 0.4, amp: 0.3
    sleep quart
    play :g, release: 0.8, amp: 0.3
    sleep 3
  end
  define :permanent_drumset do
    freak_out_mode = [true, false, false].sample
    length.to_i.times.each_with_index do |_, i|
      sample :drum_bass_soft, amp: 0.3
      sample :drum_cymbal_closed, amp: 0.3
      sleep half
    end
  end
  in_thread(name: :frame) do
    cue :frame
    12.times do
      permanent_drumset
    end
  end
  in_thread do
    12.times do
      bass_guitar
    end
  end
end

#Merag
in_thread do
  define :play_bb do |n|
    sample :ambi_drone, rate: [0.25, 0.5, 0.125, 1].choose, amp: 0.25 if rand < 0.125
    sleep sample_duration(:loop_amen) / n
  end
  loop {play_bb([1,2,4,8,16].choose)}
end
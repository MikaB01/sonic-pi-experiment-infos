in_thread do
  define :play_bb do |n|
    sample :ambi_drone, rate: [0.25, 0.5, 0.125, 1].choose, amp: 0.25 if rand < 0.125
    sleep sample_duration(:loop_amen) / n
  end
  loop {play_bb([1,2,4,8,16].choose)}
end

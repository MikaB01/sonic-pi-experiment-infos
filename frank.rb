in_thread do
  use_synth  :zawa
  sleep 2
  2.times do
    play chord(:e3, :minor)
    sleep 1
    play chord(:e3, :dim7)
    sleep 1
    play_pattern_timed chord(:f3, :minor), 0,25
    sleep 1
    play_pattern_timed chord(:b3, :minor), 2
    sleep 6
  end
end